package catStory;

import human.Master;

class HomeCat extends Cat implements Playable {
    Master master;

    public HomeCat(String name, String breed, Master master) {
        super(name, breed);
        this.master = master;
    }

    @Override
    void voice() {
        System.out.println("Meooow");
    }

    public void displayMaster() {
        System.out.println(master.getName());
    }

    public void layOnSofa() {
        System.out.println("URRRRRRRR");
    }

    public void spoilShoes(Master master) {
        System.out.print("Master mood: ");
        System.out.println(master.getMoney() - 100);
    }

    @Override
    public void play() {
        System.out.println("looks happy ^_^");
    }

    public void play(Master master) {
        System.out.println("Master mood: " + (master.getMood() + 10));
    }
}
