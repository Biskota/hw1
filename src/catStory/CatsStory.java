package catStory;

import human.Master;

public class CatsStory {
    public static void main(String[] args) {
        Master master = new Master("Alex", 1000, 0);
        HomeCat luna = new HomeCat("Luna", "Abyssinian", master);
        HomeCat catalina = new HomeCat("Catalina", "Street cat", master);
        WildCat abby = new WildCat("Abby", "Felis silvestris lybica");
        WildCat fin = new WildCat("Fin", "Felis silvestris lybica");
        System.out.println("Hello, my name is " + luna.getName() + ". I am a " + luna.getBreed() + " cat. I want to tell you one story.");
        System.out.println("This story started when cats was independent. Once upon a time when cats live at the forest and catching food in the field. ");
        System.out.println("One wild cat whose name was " + abby.getName() + ", walking and saw a village.");
        System.out.println("People in this village have a big problem. The mice have eaten the grain, people tried to kill them, but mice is too fast.");
        System.out.print(abby.getName() + " wants to help people. She calls her friend " + fin.getName() + " and they ");
        abby.saveWorld();
        System.out.print("People saw that " + abby.getName() + " help them. One little girl want to pet a cat, but " + abby.getName() + " ");
        abby.hatePeople();
        System.out.print("At the next time that girl tried to play with cat and " + abby.getName() + " wants to play with girl. Cat ");
        abby.play();
        System.out.print("Nowdays, cats have a master. My master is ");
        luna.displayMaster();
        System.out.print("When we are playing, his mood staying better. ");
        luna.play(master);
        System.out.print("But last week he brought new cat. He took her from the street. Her name " + catalina.getName() + ". She is " + catalina.getBreed() + ". I freaked out and spoil his boots. ");
        luna.spoilShoes(master);
        System.out.println("But today i can't complain, we are friend with " + catalina.getName() + ". We like laying on sofa with our master. ");
        luna.layOnSofa();
    }
}





