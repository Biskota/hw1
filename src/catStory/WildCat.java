package catStory;

class WildCat extends Cat implements Playable {

    public WildCat(String name, String breed) {
        super(name, breed);
    }

    @Override
    void voice() {
        System.out.println("Meooow");
    }
    public void hatePeople() {
        System.out.println("try to bite and hiss.");
    }

    public void saveWorld() {
        System.out.println("eat all mice.");
    }

    @Override
    public void play() {
        System.out.println("looks happy ^_^ ");
    }
}