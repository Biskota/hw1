package human;

public class Master {

    private String name;
    private int money;
    private int mood;


    public String getName() {
        return name;
    }

    public int getMoney() {
        return money;
    }

    public int getMood() {
        return mood;
    }

    public Master(String name, int money, int mood) {
        this.name = name;
        this.money = money;
        this.mood = mood;
    }

}